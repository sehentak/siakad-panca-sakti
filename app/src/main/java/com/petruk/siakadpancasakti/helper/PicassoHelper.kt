package com.petruk.siakadpancasakti.helper

import android.content.Context
import android.os.Build
import android.widget.ImageView
import com.petruk.siakadpancasakti.R

import com.squareup.picasso.Picasso

/**
 * Created by angger on 10/20/2017.
 */

object PicassoHelper {

    private fun padding(context: Context, paddingInDP: Int): Int {
        val scale = context.resources.displayMetrics.density
        return (paddingInDP * scale + 0.5f).toInt()
    }

    fun load(url: String, imageView: ImageView) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Picasso.get()
                    .load(url)
                    .fit().centerCrop()
                    .into(imageView)
        } else {
            Picasso.get()
                    .load(url)
                    .fit().centerCrop()
                    .placeholder(R.drawable.app_image_progress)
                    .error(R.drawable.ic_broken)
                    .transform(CircleTransform())
                    .into(imageView)
        }
    }
}
