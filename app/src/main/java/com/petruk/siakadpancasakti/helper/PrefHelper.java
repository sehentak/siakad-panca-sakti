package com.petruk.siakadpancasakti.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.gson.Gson;
import com.petruk.siakadpancasakti.BuildConfig;
import com.petruk.siakadpancasakti.model.UserMdl;

/**
 * Created by angger on 23/06/18.
 */

public class PrefHelper {

    private static SharedPreferences pref(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @SuppressLint("CommitPrefEdits")
    public static void saveUserID(Context context, String auth, int id) {
        pref(context).edit().putInt(BuildConfig.PREF_AUTH + auth, id).apply();
    }

    public static int getUserID(Context context, String auth) {
        return pref(context).getInt(BuildConfig.PREF_AUTH + auth, 0);
    }

    @SuppressLint("CommitPrefEdits")
    public static void saveUserData(Context context, UserMdl userMdl) {
        pref(context).edit().putString(BuildConfig.PREF_USER + getUserID(userMdl), new Gson().toJson(userMdl)).apply();
    }

    public static void saveNews(Context context, DataSnapshot news, String userCode) {
        pref(context).edit().putString(BuildConfig.PREF_NEWS + userCode, new Gson().toJson(news)).apply();
    }

    public static DataSnapshot getNews(Context context, String userCode) {
        String json = pref(context).getString(BuildConfig.PREF_NEWS + userCode, null);
        if (json != null) {
            return new Gson().fromJson(json, DataSnapshot.class);
        } else return null;
    }

    public static UserMdl getLoggedUser(Context context, UserMdl userMdl, int id) {
        String userId;
        if (id > 0) userId = String.valueOf(id);
        else userId = getUserID(userMdl);
        return new Gson().fromJson(pref(context).getString(BuildConfig.PREF_USER + userId, null), UserMdl.class);
    }

    public static void deleteAll(Context context) {
        pref(context).edit().clear().apply();
    }

    private static String getUserID(UserMdl userMdl) {
        String id = String.valueOf(userMdl.getNik());
        if (!TextUtils.isEmpty(id)) return id;
        else return String.valueOf(userMdl.getNim());
    }
}