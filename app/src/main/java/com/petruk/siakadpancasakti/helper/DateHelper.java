package com.petruk.siakadpancasakti.helper;

import android.annotation.SuppressLint;
import android.util.Log;

import com.petruk.siakadpancasakti.BuildConfig;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by angger on 08/07/18.
 */

public class DateHelper {
    private static String pattern = "dd-MMM-yyyy HH:mm";

    @SuppressLint("SimpleDateFormat")
    public static String getDateNow() {
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        Date date = new Date();
        return dateFormat.format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public static long convertStringDateToLong(String date) {
        SimpleDateFormat f = new SimpleDateFormat(pattern);
        try {
            Date d = f.parse(date);
            return d.getTime();
        } catch (ParseException e) {
            if (BuildConfig.DEBUG)
                Log.e("DateHelper", "dateLong: " + e.getMessage());
            return 0;
        }
    }
}
