package com.petruk.siakadpancasakti.helper;

public class StringHelper {
    public static final String REGEX_EMAIL =
            "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String REGEX_PHONE = "^((\\\\+62)|(00)|){0,3}[0-9]{9,13}$";

    public static final String REGEX_NUMBER = "^([signInWithNIM-zA-Z]){1}\\s?[0-9]{1,4}\\s?[signInWithNIM-zA-z]{3}$";

}