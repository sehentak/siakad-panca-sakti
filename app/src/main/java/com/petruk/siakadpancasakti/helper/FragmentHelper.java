package com.petruk.siakadpancasakti.helper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by angger on 29/05/18.
 */

public class FragmentHelper {
    public static void replaceFragment(AppCompatActivity activity, Fragment fragment, int frame) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(frame, fragment);
        transaction.commit();
    }
}