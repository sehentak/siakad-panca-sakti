package com.petruk.siakadpancasakti.core.auth.login;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BasePresenter;
import com.petruk.siakadpancasakti.model.ConnEmail;
import com.petruk.siakadpancasakti.model.ConnNIM;

public class LoginPresenter extends BasePresenter<LoginView> {
    private FirebaseDatabase db;
    private DatabaseReference ref;

    public LoginPresenter(LoginView mView) {
        super(mView);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();
    }

    public void validate(Context context, TextInputEditText username, TextInputEditText password) {
        String u = username.getText().toString().trim();
        String p = password.getText().toString().trim();
        if (!TextUtils.isEmpty(u) && !TextUtils.isEmpty(p)) {
            if (validateEmail(u)) {
                if (p.length() >= 6) {
                    signInWithEmailAndPassword(u, p);
                } else password.setError(context.getString(R.string.message_password_min));
            } else {
                if (u.matches("[0-9]+") && u.length() > 2) signInWithNIM(context, u, p);
                else username.setError(context.getString(R.string.message_email_pattern));
            }
        } else {
            String fill = context.getString(R.string.message_fill);
            if (TextUtils.isEmpty(u)) username.setError(fill);
            if (TextUtils.isEmpty(p)) password.setError(fill);
        }
    }

    private void checkActivated() {
        mView.showLoading();
//        ref = db.getReference()
    }

    private void signInWithEmailAndPassword(String email, String password) {
        mView.showLoading();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    mView.stopLoading();
                    if (task.isSuccessful()) {
                        mView.onSuccessLoginWithEmail(mAuth.getCurrentUser());
                    }
                })
                .addOnFailureListener(e -> {
                    mView.stopLoading();
                    mView.onFailedProcess(e.getMessage());
                });
    }

    private void signInWithNIM(Context context, String username, String password) {
        mView.showLoading();
        ref = db.getReference(context.getString(R.string.property_conn));
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mView.stopLoading();
                if (dataSnapshot.hasChild(username)) {
                    ConnNIM connNIM = dataSnapshot.child(username).getValue(ConnNIM.class);
                    if (connNIM != null) {
                        String email = connNIM.getEmail();
                        String newPassword = connNIM.getPassword();
                        if (newPassword == null) newPassword = password;
                        if (email != null)
                            mView.onSuccessLoginWithNIM(username, email, newPassword);
                        else mView.onSuccessLoginWithNIM(username, null, newPassword);
                    } else Toast.makeText(context, "connNIM: null", Toast.LENGTH_SHORT).show();
                } else Toast.makeText(context, "connNIM: null", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mView.stopLoading();
                mView.onFailedProcess(databaseError.getMessage());
            }
        });
    }

    public void setEmailToConnection(Context context, String username, String email, FirebaseUser user) {
        mView.showLoading();
        ref = db.getReference(context.getString(R.string.property_conn));
        ref.child(username).setValue(new ConnNIM(email))
                .addOnCompleteListener(task -> {
                    mView.stopLoading();
                    if (task.isSuccessful()) {
                        if (user == null) mView.onSuccessAddEmailToConn(email);
                        else mView.onSuccessLoginWithEmail(mAuth.getCurrentUser());
                    }
                })
                .addOnFailureListener(e -> {
                    mView.stopLoading();
                    mView.onFailedProcess(e.getMessage());
                });
    }

    public void createUserWithEmailAndPassword(Context context, String usr, String email, String password) {
        mView.showLoading();
        mAuth.createUserWithEmailAndPassword(email.replace(context.getString(R.string.property_email), ""), password)
                .addOnCompleteListener(task -> {
                    mView.stopLoading();
                    mAuth.getCurrentUser().sendEmailVerification();
                    addPassword(context, usr, email, password, mAuth.getCurrentUser());
                })
                .addOnFailureListener(e -> {
                    mView.stopLoading();
                    mView.onFailedProcess(e.getMessage());
                });
    }

    private void addPassword(Context context, String username, String email, String password, FirebaseUser user) {
        mView.showLoading();
        if (user.isEmailVerified())
            email = email.replace(context.getString(R.string.property_email), "");
        ref = db.getReference(context.getString(R.string.property_conn));
        String finalEmail = email;
        ref.child(username).setValue(new ConnNIM(finalEmail, password))
                .addOnCompleteListener(task -> {
                    mView.stopLoading();
                    if (task.isSuccessful()) {
                        setNIMToConnection(context, username, finalEmail);
                    }
                })
                .addOnFailureListener(e -> {
                    mView.stopLoading();
                    mView.onFailedProcess(e.getMessage());
                });
    }

    private void setNIMToConnection(Context context, String username, String email) {
        mView.showLoading();
        ref = db.getReference(context.getString(R.string.property_conn));
        String mEmail = removeDot(email.replace(context.getString(R.string.property_email), ""));
        ref.child(mEmail).setValue(new ConnEmail(Integer.parseInt(username)))
                .addOnCompleteListener(task -> {
                    mView.stopLoading();
                    if (task.isSuccessful()) {
                        mView.onSuccessFinal();
                    }
                })
                .addOnFailureListener(e -> {
                    mView.stopLoading();
                    mView.onFailedProcess(e.getMessage());
                });
    }

    public void forgot(Context context, TextInputEditText username) {
        String u = username.getText().toString().trim();
        if (!TextUtils.isEmpty(u)) {
            if (validateEmail(u)) {
                mView.showLoading();
                mAuth.sendPasswordResetEmail(u)
                        .addOnCompleteListener(task -> {
                            mView.stopLoading();
                            if (task.isSuccessful()) {
                                mView.onSuccessSendResetPassword(u);
                            }
                        })
                        .addOnFailureListener(e -> {
                            mView.stopLoading();
                            mView.onFailedProcess(e.getMessage());
                        });
            } else username.setError(context.getString(R.string.message_email_pattern));
        } else username.setError(context.getString(R.string.message_fill));
    }
}