package com.petruk.siakadpancasakti.core.menu.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.petruk.siakadpancasakti.BuildConfig;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BasePresenter;
import com.petruk.siakadpancasakti.helper.DateHelper;
import com.petruk.siakadpancasakti.helper.PrefHelper;
import com.petruk.siakadpancasakti.model.NewsMdl;

import java.util.ArrayList;

/**
 * Created by angger on 29/05/18.
 */

public class HomePresenter extends BasePresenter<HomeView> {
    private FirebaseDatabase db;
    private DatabaseReference ref;

    public HomePresenter(HomeView mView) {
        super(mView);
        db = FirebaseDatabase.getInstance();
    }

    public void getNewsData(Context context, String nimUser) {
        DataSnapshot news = PrefHelper.getNews(context, nimUser);
        if (news == null) {
            mView.showLoading();
            ref = db.getReference(context.getString(R.string.property_news));
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    mView.stopLoading();

                    ArrayList<NewsMdl> list = new ArrayList<>();

                    if (dataSnapshot.exists()) {
                        PrefHelper.saveNews(context, dataSnapshot, nimUser);
                        Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                        for (DataSnapshot ds : iterable) {
                            String title = (String) ds.child("title").getValue();
                            String date = (String) ds.child("date").getValue();
                            String message = (String) ds.child("message").getValue();
                            String image = (String) ds.child("image").getValue();
                            long type = (long) ds.child("type").getValue();

                            NewsMdl newsMdl = new NewsMdl();
                            newsMdl.setTitle(title);
                            newsMdl.setDate(date);
                            newsMdl.setMessage(message);
                            newsMdl.setImage(image);
                            newsMdl.setType((int) type);

                            list.add(newsMdl);
                        }
                    }

                    if (list.size() > 0)
                        if (BuildConfig.DEBUG)
                            Log.e("HomePresenter", "listData: title: " + list.get(0).getTitle() + " " + list.get(1).getTitle() + " " + list.get(2).getTitle());
                    mView.onSuccessGetNews(list);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    mView.stopLoading();
                    mView.onFailedProcess("getNewsData: ", databaseError.getMessage());
                }
            });
        } else {
            ArrayList<NewsMdl> list = new ArrayList<>();
            Iterable<DataSnapshot> iterable = news.getChildren();
            for (DataSnapshot ds : iterable) {
                String title = (String) ds.child("title").getValue();
                String date = (String) ds.child("date").getValue();
                String message = (String) ds.child("message").getValue();
                String image = (String) ds.child("image").getValue();
                long type = (long) ds.child("type").getValue();

                NewsMdl newsMdl = new NewsMdl();
                newsMdl.setTitle(title);
                newsMdl.setDate(date);
                newsMdl.setMessage(message);
                newsMdl.setImage(image);
                newsMdl.setType((int) type);

                list.add(newsMdl);
            }

            if (list.size() > 0)
                if (BuildConfig.DEBUG)
                    Log.e("HomePresenter", "listData: title: " + list.get(0).getTitle() + " " + list.get(1).getTitle() + " " + list.get(2).getTitle());
            mView.onSuccessGetNews(list);
        }
    }

    public void setNewsData(Context context, String title, String message) {
        mView.showLoading();
        String currentDateTime = DateHelper.getDateNow();
        String id = String.valueOf(DateHelper.convertStringDateToLong(currentDateTime));
        ref = db.getReference(context.getString(R.string.property_news));
        ref.child(id).setValue(new NewsMdl(title, message, null, currentDateTime, 0))
                .addOnCompleteListener(task -> {
                    mView.stopLoading();
                    mView.onSuccessPostNews();
                })
                .addOnFailureListener(e -> {
                    mView.stopLoading();
                    mView.onFailedProcess("setNewsData: ", e.getMessage());
                });
    }
}