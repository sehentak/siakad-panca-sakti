package com.petruk.siakadpancasakti.core.menu.guidance;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BaseFragment;

import butterknife.ButterKnife;

/**
 * Created by angger on 29/05/18.
 */

public class GuidanceFragment extends BaseFragment {

    public static GuidanceFragment newsInstance() {
        GuidanceFragment fragment = new GuidanceFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.menu_guidance, container, false);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }
}