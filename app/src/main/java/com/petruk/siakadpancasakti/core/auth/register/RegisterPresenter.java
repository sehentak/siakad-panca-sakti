package com.petruk.siakadpancasakti.core.auth.register;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BasePresenter;
import com.petruk.siakadpancasakti.model.AdminRegisterMdl;
import com.petruk.siakadpancasakti.model.ConnNIM;
import com.petruk.siakadpancasakti.model.UserMdl;

public class RegisterPresenter extends BasePresenter<RegisterView> {
    private String emptyString = "empty";
    private FirebaseDatabase db;
    private DatabaseReference ref;

    public RegisterPresenter(RegisterView mView) {
        super(mView);
        db = FirebaseDatabase.getInstance();
    }

    public void validate(Context context, TextInputEditText nim, TextInputEditText name, int program, int role) {
        String sNim = nim.getText().toString().trim();
        String sName = name.getText().toString().trim();
        String sProgram = String.valueOf(program);
        String sAdmin = context.getString(R.string.property_admin);
        if (!TextUtils.isEmpty(sNim) && !TextUtils.isEmpty(sName)) {
            if (role == 1) {
                if (sName.contains(sAdmin)) {
                    sName = sName.replace(sAdmin, "");
                    AdminRegisterMdl modelAdmin = new AdminRegisterMdl(sNim, sName, program, role);
                    mView.checkPinCode(modelAdmin);
                } else createConnection(context, sNim, sName, program, role);
            } else createConnection(context, sNim, sName, program, role);
        } else {
            String fill = context.getString(R.string.message_fill);
            if (TextUtils.isEmpty(sNim)) nim.setError(fill);
            if (TextUtils.isEmpty(sName)) name.setError(fill);
            if (TextUtils.isEmpty(sProgram))
                toast(context, context.getString(R.string.message_pick_program));
        }
    }

    private void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void createConnection(Context context, String nim, String nama, int program, int role) {
        mView.showLoading();
        ref = db.getReference(context.getString(R.string.property_conn));
        ref.child(nim).setValue(new ConnNIM(emptyString))
                .addOnCompleteListener(task -> {
                    mView.stopLoading();
                    createData(context, nim, nama, program, role);
                })
                .addOnFailureListener(e -> {
                    mView.stopLoading();
                    mView.onFailedProcess(e.getMessage());
                });
    }

    private void createData(Context context, String nim, String nama, int program, int role) {
        int iNIM = -1, iNIK = -1;
        if (role == 0 || role == 1) iNIK = Integer.parseInt(nim);
        else if (role == 2) iNIM = Integer.parseInt(nim);

        mView.showLoading();
        ref = db.getReference(context.getString(R.string.property_user));
        int emptyInteger = 0;
        UserMdl user = new UserMdl(
                iNIM,
                nama,
                emptyString,
                emptyString,
                emptyString,
                emptyString,
                emptyString,
                iNIK,
                emptyInteger,
                emptyInteger,
                emptyInteger,
                emptyInteger,
                emptyString,
                emptyString,
                emptyString,
                emptyString,
                emptyInteger,
                emptyInteger,
                emptyString,
                emptyInteger,
                emptyString,
                program,
                role,
                emptyString
        );
        ref.child(nim).setValue(user)
                .addOnCompleteListener(task -> {
                    mView.stopLoading();
                    if (task.isSuccessful())
                        mView.onSuccessRegister(user);
                })
                .addOnFailureListener(e -> {
                    mView.stopLoading();
                    mView.onFailedProcess(e.getMessage());
                });
    }
}