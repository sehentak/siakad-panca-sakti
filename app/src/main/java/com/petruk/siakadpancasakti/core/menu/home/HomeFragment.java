package com.petruk.siakadpancasakti.core.menu.home;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.petruk.siakadpancasakti.BuildConfig;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BaseFragment;
import com.petruk.siakadpancasakti.core.main.MainActivity;
import com.petruk.siakadpancasakti.model.NewsMdl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by angger on 29/05/18.
 */

public class HomeFragment extends BaseFragment implements HomeView {

    @BindView(R.id.home_list)
    RecyclerView newsList;
    private HomePresenter presenter;
    private ListAdapterNews adapterNews;
    private List<NewsMdl> mList = new ArrayList<>();
    private String userCode = null;

    public static HomeFragment newsInstance() {
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.menu_home, container, false);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        presenter = new HomePresenter(this);
        userCode = MainActivity.userMdl.getEmail().split("@")[0];

        initDataNews();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getNewsData(getContext(), userCode);
    }

    @Override
    public void showLoading() {
        baseLoading(true);
    }

    @Override
    public void stopLoading() {
        baseLoading(false);
    }

    @SuppressLint("InflateParams")
    @Optional
    @OnClick(R.id.home_fab)
    public void onClickAddNews() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        View dialogView = getLayoutInflater().inflate(R.layout.app_news_create, null);
        builder.setView(dialogView);
        builder.setTitle(getActivity().getString(R.string.label_add_new));

        TextInputEditText title = dialogView.findViewById(R.id.news_add_title);
        TextInputEditText message = dialogView.findViewById(R.id.news_add_message);

        builder.setPositiveButton(getActivity().getString(R.string.label_news_post),
                (dialog, which) -> {
                    String sTitle = title.getText().toString().trim();
                    String sMessage = message.getText().toString().trim();
                    if (!TextUtils.isEmpty(sTitle) && !TextUtils.isEmpty(sMessage)) {
                        presenter.setNewsData(getActivity(), sTitle, sMessage);
                    } else {
                        String fillIt = getActivity().getString(R.string.message_fill);
                        if (TextUtils.isEmpty(sTitle)) title.setError(fillIt);
                        if (TextUtils.isEmpty(sMessage)) message.setError(fillIt);
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void initDataNews() {
        newsList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        adapterNews = new ListAdapterNews(mList);
        newsList.setAdapter(adapterNews);
        newsList.setHasFixedSize(true);
    }

    @Override
    public void onSuccessGetNews(List<NewsMdl> newsMdls) {
        if (newsMdls != null) {
            if (BuildConfig.DEBUG)
                Log.e("HomeFragment", "newsModel size: " + newsMdls.size() + ", data:\n" + Arrays.toString(newsMdls.toArray()));
            mList.clear();
            mList.addAll(newsMdls);
            adapterNews.notifyDataSetChanged();
        } else if (BuildConfig.DEBUG) Log.e("HomeFragment", "newsModel size: empty");
    }

    @Override
    public void onSuccessPostNews() {
        presenter.getNewsData(getActivity(), userCode);
    }

    @Override
    public void onFailedProcess(String tag, String errorMessage) {
        if (BuildConfig.DEBUG)
            Log.e("HomeFragment", tag + errorMessage);
        else toast(getActivity().getString(R.string.message_error));
    }
}