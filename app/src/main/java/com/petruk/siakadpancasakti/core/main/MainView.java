package com.petruk.siakadpancasakti.core.main;

import com.petruk.siakadpancasakti.base.BaseView;
import com.petruk.siakadpancasakti.model.UserMdl;

public interface MainView extends BaseView {
    void onSuccessGetUserData(UserMdl userMdl);

    void onSuccessGetNIM(int NIM);

    void onSuccessSendVerification();

    void onFailedProcess(String errorMessage);
}