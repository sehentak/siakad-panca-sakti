package com.petruk.siakadpancasakti.core.auth.login;

import com.google.firebase.auth.FirebaseUser;
import com.petruk.siakadpancasakti.base.BaseView;

public interface LoginView extends BaseView {
    void onSuccessLoginWithEmail(FirebaseUser user);

    void onSuccessLoginWithNIM(String username, String email, String password);

    void onSuccessAddEmailToConn(String email);
    void onSuccessSendResetPassword(String username);

    void onSuccessFinal();
    void onFailedProcess(String errorMessage);
}