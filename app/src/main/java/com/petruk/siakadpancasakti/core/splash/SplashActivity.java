package com.petruk.siakadpancasakti.core.splash;

import android.os.Bundle;
import android.widget.TextView;

import com.petruk.siakadpancasakti.BuildConfig;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BaseActivity;
import com.petruk.siakadpancasakti.core.auth.login.LoginActivity;
import com.petruk.siakadpancasakti.core.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashView {
    private SplashPresenter presenter;

    @BindView(R.id.splash_version)
    TextView version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        baseFullScreen();
        setContentView(R.layout.splash_activity);
        ButterKnife.bind(this);
        presenter = new SplashPresenter(this);
        version.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.checkUserActive();
    }

    @Override
    public void userHasLogged() {
        baseIntent(MainActivity.class, true);
    }

    @Override
    public void userNoLogged() {
        baseIntent(LoginActivity.class, true);
    }

    @Override
    public void showLoading() {
        baseLoading(true);
    }

    @Override
    public void stopLoading() {
        baseLoading(false);
    }
}