package com.petruk.siakadpancasakti.core.menu.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.petruk.siakadpancasakti.BuildConfig;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.helper.PicassoHelper;
import com.petruk.siakadpancasakti.model.NewsMdl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by angger on 08/07/18.
 */

public class ListAdapterNews extends RecyclerView.Adapter<ListAdapterNews.ViewHolder> {

    private List<NewsMdl> newsMdls;

    public ListAdapterNews(List<NewsMdl> newsMdls) {
        this.newsMdls = newsMdls;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.app_item_news, parent, false);
        ButterKnife.bind(this, view);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        NewsMdl model = newsMdls.get(position);
        holder.title.setText(model.getTitle());
        holder.date.setText(model.getDate());
        holder.message.setText(model.getMessage());
        String path = model.getImage();
        if (path != null) PicassoHelper.INSTANCE.load(path, holder.image);
        else holder.image.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        int size = newsMdls.size();
        if (BuildConfig.DEBUG)
            Log.e("ListAdapterNews", "size: " + size);
        return size;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.news_title)
        TextView title;
        @BindView(R.id.news_date)
        TextView date;
        @BindView(R.id.news_message)
        TextView message;
        @BindView(R.id.news_image)
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
