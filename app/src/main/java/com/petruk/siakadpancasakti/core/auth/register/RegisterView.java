package com.petruk.siakadpancasakti.core.auth.register;

import com.petruk.siakadpancasakti.base.BaseView;
import com.petruk.siakadpancasakti.model.AdminRegisterMdl;
import com.petruk.siakadpancasakti.model.UserMdl;

public interface RegisterView extends BaseView {
    void checkPinCode(AdminRegisterMdl model);
    void onSuccessRegister(UserMdl user);
    void onFailedProcess(String errorMessage);
}