package com.petruk.siakadpancasakti.core.splash;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.petruk.siakadpancasakti.base.BasePresenter;

public class SplashPresenter extends BasePresenter<SplashView> {
    public SplashPresenter(SplashView mView) {
        super(mView);
        mAuth = FirebaseAuth.getInstance();
    }

    public void checkUserActive() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) mView.userHasLogged();
        else mView.userNoLogged();
    }
}