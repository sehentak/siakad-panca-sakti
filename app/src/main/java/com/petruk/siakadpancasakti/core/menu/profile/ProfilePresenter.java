package com.petruk.siakadpancasakti.core.menu.profile;

import com.petruk.siakadpancasakti.base.BasePresenter;

/**
 * Created by angger on 29/05/18.
 */

public class ProfilePresenter extends BasePresenter<ProfileView> {
    public ProfilePresenter(ProfileView mView) {
        super(mView);
    }
}