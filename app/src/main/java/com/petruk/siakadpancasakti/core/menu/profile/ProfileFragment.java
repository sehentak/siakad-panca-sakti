package com.petruk.siakadpancasakti.core.menu.profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.petruk.siakadpancasakti.BuildConfig;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BaseFragment;
import com.petruk.siakadpancasakti.core.main.MainActivity;
import com.petruk.siakadpancasakti.helper.ImageUtils;
import com.petruk.siakadpancasakti.helper.PicassoHelper;
import com.petruk.siakadpancasakti.helper.PrefHelper;
import com.petruk.siakadpancasakti.model.UserMdl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

import static android.app.Activity.RESULT_OK;

/**
 * Created by angger on 29/05/18.
 */

public class ProfileFragment extends BaseFragment implements ProfileView {

    @BindView(R.id.profile_img_avatar)
    ImageView avatar;
    @BindView(R.id.profile_tv_address)
    TextView address1;
    @BindView(R.id.profile_tv_address2)
    TextView address2;
    @BindView(R.id.profile_tv_address3)
    TextView address3;
    @BindView(R.id.profile_tv_name)
    TextView name;
    @BindView(R.id.profile_tv_nik)
    TextView nik;
    @BindView(R.id.profile_tv_email)
    TextView email;
    @BindView(R.id.profile_tv_type)
    TextView type;
    @BindView(R.id.profile_tv_address4)
    TextView address4;
    @BindView(R.id.profile_btn_logout)
    Button btnLogout;
    @BindView(R.id.profile_btn_edit)
    ImageButton btnEdit;
    @BindView(R.id.profile_lin_address)
    LinearLayout linAddress;

    private ProfilePresenter presenter;
    private File file = null;
    private UserMdl userMdl;
    private String PROPERTY_EMPTY = "empty";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.menu_profile, container, false);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new ProfilePresenter(this);
        ButterKnife.bind(this, view);

        initUser();
    }

    private void initUser() {
        try {
            userMdl = MainActivity.userMdl;
            name.setText(userMdl.getNama());
            int number = userMdl.getNik();
            if (number == -1) number = userMdl.getNim();
            nik.setText(String.valueOf(number));
            String emailAddress = userMdl.getEmail().matches(PROPERTY_EMPTY) ? "Not verified yet" : userMdl.getEmail();
            email.setText(emailAddress);
            type.setText(userType(userMdl));

            String ava = userMdl.getAvatar();
            if (ava.matches(PROPERTY_EMPTY))
                avatar.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_account));
            else PicassoHelper.INSTANCE.load(userMdl.getAvatar(), avatar);

            String alamat = checkData(userMdl.getAlamat());
            String kecamatan = checkData(userMdl.getKecamatan());
            String kelurahan = checkData(userMdl.getKelurahan());
            String kodePos = checkData(userMdl.getKodePos());
            if (TextUtils.isEmpty(alamat) && TextUtils.isEmpty(kecamatan) &&
                    TextUtils.isEmpty(kelurahan) && TextUtils.isEmpty(kodePos)) {
                linAddress.setVisibility(View.GONE);
                btnEdit.setVisibility(View.GONE);
                btnLogout.setVisibility(View.GONE);
            } else {
                checkData(address1, alamat);
                checkData(address2, kecamatan);
                checkData(address3, kelurahan);
                checkData(address4, kodePos);
            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) Log.e("ProfileFragment", "InitUser:\nerror: " + e.getMessage());
            btnLogout.performClick();
            toast("Please try to login");
        }
    }

    private String checkData(String data) {
        return data.matches(PROPERTY_EMPTY) ? null : data;
    }

    private void checkData(TextView textView, String data) {
        if (TextUtils.isEmpty(data))
            textView.setVisibility(View.GONE);
        else textView.setText(data);
    }

    private String userType(UserMdl userMdl) {
        int role = userMdl.getRole();
        if (role == 0) return "Admin";
        else if (role == 1) return "Lecture";
        else if (role == 2) return "Student";
        else return "";
    }

    @Optional
    @OnClick(R.id.profile_btn_edit)
    public void onClickEdit() {
        toast("edit cuy");
    }

    @OnClick(R.id.profile_btn_logout)
    public void onClickLogout() {
        PrefHelper.deleteAll(getActivity());
        MainActivity.logout();
    }

    @OnClick(R.id.profile_img_avatar)
    public void onClickAvatar() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                )
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                            , android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        else {


            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = ImageUtils.getTempFile(getActivity());
            Uri photoURI;
            try {
                if (Build.VERSION.SDK_INT >= 24) {
                    photoURI = FileProvider.getUriForFile(getActivity(),
                            BuildConfig.APPLICATION_ID + ".provider", file);
                    takePhotoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                } else photoURI = Uri.fromFile(file);
            } catch (IllegalArgumentException e) {
                Log.d("ProfileFragment", "CameraOpenAvatar: " + e.getMessage());
                photoURI = null;
            }

            takePhotoIntent.putExtra("return-data", true);
            if (photoURI != null) takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePhotoIntent, 4);
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1337:
                if (resultCode == RESULT_OK) {
                    if (file != null) {
                        Bitmap takenImage = BitmapFactory.decodeFile(file.getAbsolutePath());
                        avatar.setImageBitmap(takenImage);
                    }
                }
                break;
            case 4:
                if (resultCode == RESULT_OK) {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
                    String fileName = userMdl.getNik() + "_" + timeStamp + ".jpg";
                    File f = ImageUtils.getImage(getActivity(), fileName);
                    toast(String.valueOf(f));
                }
                break;
        }
    }

    @Override
    public void showLoading() {
        baseLoading(true);
    }

    @Override
    public void stopLoading() {
        baseLoading(false);
    }
}