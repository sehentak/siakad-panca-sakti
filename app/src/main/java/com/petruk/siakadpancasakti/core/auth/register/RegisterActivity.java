package com.petruk.siakadpancasakti.core.auth.register;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.petruk.siakadpancasakti.BuildConfig;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BaseActivity;
import com.petruk.siakadpancasakti.core.auth.login.LoginActivity;
import com.petruk.siakadpancasakti.core.main.MainActivity;
import com.petruk.siakadpancasakti.model.AdminRegisterMdl;
import com.petruk.siakadpancasakti.model.UserMdl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements RegisterView {
    @BindView(R.id.auth_register_edt_user_nama)
    TextInputEditText name;
    private RegisterPresenter presenter;
    private int radioProgram = -1;

    @BindView(R.id.auth_register_lin_group_data)
    LinearLayout groupLinearData;
    @BindView(R.id.auth_register_lin_group_program)
    LinearLayout groupLinearProgram;
    @BindView(R.id.auth_register_edt_user_nim)
    TextInputEditText nim;
    private int radioRole = -1;
    @BindView(R.id.auth_register_radio_group_role)
    RadioGroup groupRadioRole;
    @BindView(R.id.auth_register_radio_group_program)
    RadioGroup groupRadioProgram;
    @BindView(R.id.auth_register_btn_process)
    Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_register);
        ButterKnife.bind(this);
        presenter = new RegisterPresenter(this);

        registerButton.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_NEXT) {
                registerButton.performClick();
                return true;
            } else return false;
        });

        initState();
    }

    private void initState() {
        groupRadioRole.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.auth_register_radio_dosen) {
                radioRole = 1;
                groupLinearProgram.setVisibility(View.GONE);
                defaultState();
            } else if (checkedId == R.id.auth_register_radio_mahasiswa) {
                radioRole = 2;
                groupLinearProgram.setVisibility(View.VISIBLE);
                defaultState();
            }
        });

        groupRadioProgram.setOnCheckedChangeListener((group1, checkedId) -> {
            if (checkedId == R.id.auth_register_radio_karyawan) radioProgram = 0;
            else if (checkedId == R.id.auth_register_radio_pagi) radioProgram = 1;
            else if (checkedId == R.id.auth_register_radio_malam) radioProgram = 2;
        });
    }

    private void defaultState() {
        groupLinearData.setVisibility(View.VISIBLE);
        groupRadioProgram.clearCheck();
        radioProgram = -1;
        nim.setText("");
        name.setText("");
    }

    @Override
    public void onBackPressed() {
        baseIntent(MainActivity.class, true);
    }

    @OnClick(R.id.auth_register_btn_process)
    public void onClickProcess() {
        if (radioRole == -1 || radioRole == 0) baseToast(getString(R.string.message_pick_role));
        else presenter.validate(this, nim, name, radioProgram, radioRole);
    }

    @SuppressLint("InflateParams")
    @Override
    public void checkPinCode(AdminRegisterMdl model) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.app_dialog_edit, null);
        EditText apcode = dialogView.findViewById(R.id.app_edit_text);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton(getString(R.string.label_process), (dialog, which) -> {
            String sApCode = apcode.getText().toString().trim();
            dialog.dismiss();
            if (!TextUtils.isEmpty(sApCode) && sApCode.matches(BuildConfig.AP_CODE))
                presenter.createConnection(this, model.getNim(), model.getName(), model.getProgram(), 0);
            else baseToast(getString(R.string.message_no_access));
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setTitle(getString(R.string.message_input_apcode));
        alertDialog.show();
    }

    @Override
    public void onSuccessRegister(UserMdl user) {
        String register = getString(R.string.text_register);
        if (user != null) {
            baseToast(register + " " + user.getNama() + " " + getString(R.string.action_success));
            baseIntent(LoginActivity.class, true);
        } else baseToast(register + " " + getString(R.string.action_fail));
    }

    @Override
    public void onFailedProcess(String errorMessage) {
        baseToast(errorMessage);
    }

    @Override
    public void showLoading() {
        baseLoading(true);
    }

    @Override
    public void stopLoading() {
        baseLoading(false);
    }
}