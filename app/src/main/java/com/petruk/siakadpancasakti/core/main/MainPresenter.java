package com.petruk.siakadpancasakti.core.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.petruk.siakadpancasakti.BuildConfig;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BasePresenter;
import com.petruk.siakadpancasakti.helper.PrefHelper;
import com.petruk.siakadpancasakti.model.ConnEmail;
import com.petruk.siakadpancasakti.model.ConnNIM;
import com.petruk.siakadpancasakti.model.UserMdl;

public class MainPresenter extends BasePresenter<MainView> {
    private FirebaseDatabase db;
    private FirebaseUser user;
    private DatabaseReference ref;

    public FirebaseAuth auth = mAuth;

    public MainPresenter(MainView mView) {
        super(mView);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        db = FirebaseDatabase.getInstance();
    }

    public boolean isUserAvailable() {
        return user != null;
    }

    public boolean checkEmailVerified() {
        user.reload();
        boolean result = user.isEmailVerified();
        if (BuildConfig.DEBUG) Log.d("MainPresenter", "isEmailVerified: " + result);
        return result;
    }

    public void sendVerificationEmail() {
        user.sendEmailVerification()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful())
                        mView.onSuccessSendVerification();
                })
                .addOnFailureListener(e -> mView.onFailedProcess("sendVerificationEmail: " + e.getMessage()));
    }

    public void checkEmailActivation(Context context) {
        int id = PrefHelper.getUserID(context, user.getUid());
        if (BuildConfig.DEBUG) Log.d("MainPresenter", "userID: pref: " + id);
        if (id > 0) {
            mView.onSuccessGetNIM(id);
        } else {
            mView.showLoading();
            ref = db.getReference(context.getString(R.string.property_conn));
            Query query = ref.child(removeDot(user.getEmail())).orderByChild("nim");
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        ConnEmail get = dataSnapshot.getValue(ConnEmail.class);
                        int nim = get.getNim();
                        PrefHelper.saveUserID(context, user.getUid(), nim);
                        if (BuildConfig.DEBUG) Log.d("MainPresenter", "userID: api: " + id);
                        mView.onSuccessGetNIM(nim);
                    } else {
                        mView.stopLoading();
                        mView.onFailedProcess("checkEmailActivation: dataSnapshot: null");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    mView.stopLoading();
                    mView.onFailedProcess("checkEmailActivation: " + databaseError.getMessage());
                }
            });
        }
    }

    public void getUserData(Context context, int NIM) {
        UserMdl userMdl = PrefHelper.getLoggedUser(context, null, NIM);
        if (userMdl == null) {
            ref = db.getReference(context.getString(R.string.property_user));
            ref.child(String.valueOf(NIM)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    mView.stopLoading();
                    if (dataSnapshot.exists()) {
                        UserMdl userMdl = dataSnapshot.getValue(UserMdl.class);
                        PrefHelper.saveUserData(context, userMdl);
                        if (BuildConfig.DEBUG)
                            Log.d("MainPresenter", "userData: api: " + userMdl.getNama());
                        mView.onSuccessGetUserData(userMdl);
                    } else mView.onFailedProcess("checkEmailActivation: dataSnapshot: null");
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    mView.stopLoading();
                    mView.onFailedProcess("getUserData: " + databaseError.getMessage());
                }
            });
        } else {
            mView.stopLoading();
            mView.onSuccessGetUserData(userMdl);
            PrefHelper.saveUserData(context, userMdl);
            if (BuildConfig.DEBUG) Log.d("MainPresenter", "userData: pref: " + userMdl.getNama());
        }
    }

    public void changeStateEmail(Context context, int NIM, UserMdl userMdl) {
        if (userMdl.getEmail().contains(context.getString(R.string.property_email)) ||
                userMdl.getEmail().contains("empty")) {
            ref = db.getReference(context.getString(R.string.property_conn));
            ConnNIM connNIM = new ConnNIM();
            if (!TextUtils.isEmpty(connNIM.getPassword())) {
                ref.child(String.valueOf(NIM)).setValue(new ConnNIM(user.getEmail(), connNIM.getPassword()))
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful())
                                changeUserEmail(context, NIM, userMdl);
                            else mView.onFailedProcess("changeStateEmail: tast: not success");
                        })
                        .addOnFailureListener(e -> {
                            mView.onFailedProcess("changeStateEmail: " + e.getMessage());
                        });
            }
        }
    }

    public void getNewsData(Context context, String userCode) {
        DataSnapshot news = PrefHelper.getNews(context, userCode);
        if (news != null) {
            ref = db.getReference(context.getString(R.string.property_news));
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        PrefHelper.saveNews(context, dataSnapshot, userCode);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    private void changeUserEmail(Context context, int NIM, UserMdl userMdl) {
        ref = db.getReference(context.getString(R.string.property_user));
        userMdl.setEmail(user.getEmail());
        ref.child(String.valueOf(NIM)).setValue(userMdl);
    }

    public void signOut() {
        mAuth.signOut();
        user = null;
        mAuth = null;
    }
}