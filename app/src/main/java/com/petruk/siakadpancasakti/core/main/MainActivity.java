package com.petruk.siakadpancasakti.core.main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BaseActivity;
import com.petruk.siakadpancasakti.core.auth.login.LoginActivity;
import com.petruk.siakadpancasakti.core.menu.guidance.GuidanceFragment;
import com.petruk.siakadpancasakti.core.menu.home.HomeFragment;
import com.petruk.siakadpancasakti.core.menu.profile.ProfileFragment;
import com.petruk.siakadpancasakti.core.menu.schedule.ScheduleFragment;
import com.petruk.siakadpancasakti.model.UserMdl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("StaticFieldLeak")
public class MainActivity extends BaseActivity implements MainView {
    private MainPresenter presenter;
    private int NIM;
    private MenuItem home, guidance, schedule, profile;
    public static UserMdl userMdl = null;
    private static Context context;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_navigation)
    BottomNavigationViewEx navigationViewEx;
    @BindView(R.id.toolbar_title)
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);

        presenter = new MainPresenter(this);
        if (presenter.isUserAvailable()) onCreate();
        else baseIntent(LoginActivity.class, true);
    }

    public static void logout() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth != null) auth.signOut();
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    private void initNavigation() {
        navigationViewEx.enableAnimation(false);
        navigationViewEx.enableShiftingMode(false);
        navigationViewEx.enableItemShiftingMode(false);

        home = navigationViewEx.getMenu().getItem(0);
        guidance = navigationViewEx.getMenu().getItem(1);
        schedule = navigationViewEx.getMenu().getItem(2);
        profile = navigationViewEx.getMenu().getItem(3);

        navigationViewEx.setOnNavigationItemSelectedListener(item -> {
            menuPicker(item);
            return true;
        });

        menuPicker(home);
    }

    private void menuPicker(MenuItem item) {
        if (item != null) {
            int id = item.getItemId();
            Fragment fragment;
            if (id > -1) {
                switch (id) {
                    case R.id.action_home:
                        home.setEnabled(false);
                        guidance.setEnabled(true);
                        schedule.setEnabled(true);
                        profile.setEnabled(true);
                        fragment = new HomeFragment();
                        title.setText(getString(R.string.app_name));
                        break;
                    case R.id.action_guidance:
                        home.setEnabled(true);
                        guidance.setEnabled(false);
                        schedule.setEnabled(true);
                        profile.setEnabled(true);
                        fragment = new GuidanceFragment();
                        title.setText(getString(R.string.menu_guidance));
                        break;
                    case R.id.action_schedule:
                        home.setEnabled(true);
                        guidance.setEnabled(true);
                        schedule.setEnabled(false);
                        profile.setEnabled(true);
                        fragment = new ScheduleFragment();
                        title.setText(getString(R.string.menu_schedule));
                        break;
                    case R.id.action_profile:
                        home.setEnabled(true);
                        guidance.setEnabled(true);
                        schedule.setEnabled(true);
                        profile.setEnabled(false);
                        fragment = new ProfileFragment();
                        title.setText(getString(R.string.menu_profile));
                        break;
                    default:
                        home.setEnabled(false);
                        guidance.setEnabled(true);
                        schedule.setEnabled(true);
                        profile.setEnabled(true);
                        fragment = new HomeFragment();
                        title.setText(getString(R.string.app_name));
                        break;
                }

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_frame, fragment);
                transaction.commit();
            }
        }
    }

    @OnClick(R.id.main_btn_sign_out)
    public void onClickSignOut() {
        presenter.signOut();
        NIM = -1;
        presenter = null;
        baseIntent(LoginActivity.class, true);
    }

    @Override
    public void onSuccessGetUserData(UserMdl userMdl) {
        boolean isVerified = presenter.checkEmailVerified();
        if (!isVerified) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            alertDialog.setTitle("Email belum di verifikasi");
            alertDialog.setMessage("Luangkan waktu sejenak untuk verifikasi email anda.\nIni berguna untuk verifikasi kepemilikan email anda.");
            alertDialog.setPositiveButton("Verifikasi sekarang", (dialog, which) -> {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_APP_EMAIL);
                startActivity(intent);
            });
            alertDialog.setNegativeButton("Kirim verifikasi ulang", (dialog, which) -> {
                dialog.dismiss();
                presenter.sendVerificationEmail();
            });
            alertDialog.create();
            alertDialog.show();
        } else {
            presenter.changeStateEmail(this, NIM, userMdl);
        }

        MainActivity.userMdl = userMdl;
        presenter.getNewsData(this, userMdl.getEmail().split("@")[0]);
    }

    @Override
    public void onSuccessGetNIM(int NIM) {
        this.NIM = NIM;
        presenter.getUserData(this, NIM);
    }

    @Override
    public void onSuccessSendVerification() {
        baseToast("Verifikasi email telah terkirim.\nMohon check email anda");
    }

    @Override
    public void onFailedProcess(String errorMessage) {
        baseToast(errorMessage);
    }

    @Override
    public void showLoading() {
        baseLoading(true);
    }

    @Override
    public void stopLoading() {
        baseLoading(false);
    }

    private void onCreate() {
        context = this;
        ButterKnife.bind(this);
        presenter.checkEmailActivation(this);

        initNavigation();
    }
}