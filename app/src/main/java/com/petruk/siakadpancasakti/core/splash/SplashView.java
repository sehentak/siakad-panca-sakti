package com.petruk.siakadpancasakti.core.splash;

import com.petruk.siakadpancasakti.base.BaseView;

public interface SplashView extends BaseView {
    void userHasLogged();
    void userNoLogged();
}