package com.petruk.siakadpancasakti.core.menu.home;

import com.petruk.siakadpancasakti.base.BaseView;
import com.petruk.siakadpancasakti.model.NewsMdl;

import java.util.List;

/**
 * Created by angger on 29/05/18.
 */

public interface HomeView extends BaseView {
    void onSuccessGetNews(List<NewsMdl> newsMdls);

    void onSuccessPostNews();

    void onFailedProcess(String tag, String errorMessage);
}