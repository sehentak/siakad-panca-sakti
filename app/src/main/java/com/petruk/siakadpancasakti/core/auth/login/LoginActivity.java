package com.petruk.siakadpancasakti.core.auth.login;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseUser;
import com.petruk.siakadpancasakti.BuildConfig;
import com.petruk.siakadpancasakti.R;
import com.petruk.siakadpancasakti.base.BaseActivity;
import com.petruk.siakadpancasakti.core.auth.register.RegisterActivity;
import com.petruk.siakadpancasakti.core.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView {
    private LoginPresenter presenter;
    private String usr;

    @BindView(R.id.auth_login_edt_username)
    TextInputEditText username;
    @BindView(R.id.auth_login_edt_password)
    TextInputEditText password;
    @BindView(R.id.auth_login_btn_process)
    Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_login);
        ButterKnife.bind(this);
        presenter = new LoginPresenter(this);

        loginButton.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_NEXT) {
                loginButton.performClick();
                return true;
            } else return false;
        });
    }

    @OnClick(R.id.auth_login_btn_process)
    public void onClickProcess() {
        presenter.validate(this, username, password);
    }

    @OnClick(R.id.auth_login_btn_forgot)
    public void onClickForgot() {
        presenter.forgot(this, username);
    }

    @OnClick(R.id.auth_login_btn_register)
    public void onClickRegister() {
        baseIntent(RegisterActivity.class, true);
    }

    @Override
    public void onSuccessLoginWithEmail(FirebaseUser user) {
        if (user != null) baseIntent(MainActivity.class, true);
    }

    @Override
    public void onSuccessLoginWithNIM(String sUsername, String sEmail, String sPassword) {
        usr = sUsername;
        if (sEmail == null || !sEmail.contains("@")) showDialog(sUsername, true);
        else {
            String isNotVerify = getString(R.string.property_email);
            if (sEmail.contains(isNotVerify)) sEmail = sEmail.replace(isNotVerify, "");
            username.setText(sEmail);
            password.setText(sPassword);
            presenter.validate(this, username, password);
        }
    }

    @Override
    public void onSuccessAddEmailToConn(String email) {
        if (email != null) {
            showDialog(email, false);
        }
    }

    @SuppressLint("InflateParams")
    private void showDialog(String sUsername, boolean isSetEmail) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.app_dialog_edit, null);
        EditText editText = dialogView.findViewById(R.id.app_edit_text);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton(getString(R.string.label_process), (dialog, which) -> {
            dialog.dismiss();
            if (isSetEmail)
                presenter.setEmailToConnection(this, sUsername, getString(R.string.property_email) + editText.getText().toString().trim(), null);
            else {
                if (editText.getText().toString().trim().length() >= 6)
                    presenter.createUserWithEmailAndPassword(this, usr, sUsername, editText.getText().toString().trim());
                else baseToast(getString(R.string.message_password_min));
            }

        });
        AlertDialog alertDialog = dialogBuilder.create();
        String title, message;
        if (isSetEmail) {
            title = "Set email address";
            message = "Sistem ini memerlukan alamat email untuk memudahkan penggunaannya. Seperti reset password, pengiriman pengumuman dll.";
        } else {
            title = "Set new password";
            message = "Buatlah password baru untuk menjaga keamanan akun anda dari tangan yang tidak bertanggung jawab. Password minimal 6 karakter.";
        }
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.show();
    }

    @Override
    public void onSuccessSendResetPassword(String username) {
        if (!TextUtils.isEmpty(username))
            baseToast(getString(R.string.message_success_send_reset) + "\n" + username);
    }

    @Override
    public void onSuccessFinal() {
        baseToast("Please check your email address to email confirmation");
        String email = username.getText().toString().trim();
        if (!TextUtils.isEmpty(email) && email.matches("\\d+")) {
            loginButton.performClick();
        }
    }

    @Override
    public void onFailedProcess(String errorMessage) {
        if (BuildConfig.DEBUG) baseToast(errorMessage);
    }

    @Override
    public void showLoading() {
        baseLoading(true);
    }

    @Override
    public void stopLoading() {
        baseLoading(false);
    }
}