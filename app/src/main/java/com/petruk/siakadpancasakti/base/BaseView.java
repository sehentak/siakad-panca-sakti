package com.petruk.siakadpancasakti.base;

public interface BaseView {
    void showLoading();

    void stopLoading();
}