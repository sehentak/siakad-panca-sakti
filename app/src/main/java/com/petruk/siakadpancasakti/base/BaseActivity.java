package com.petruk.siakadpancasakti.base;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.petruk.siakadpancasakti.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/nunito_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
    }

    public void baseFullScreen() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("InflateParams")
    public void baseLoading(boolean mode) {
        if (mode) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.app_loading, null);
            dialogBuilder.setView(dialogView);
            alertDialog = dialogBuilder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setCancelable(false);
            alertDialog.show();
        } else {
            if (alertDialog != null) {
                alertDialog.hide();
                alertDialog.dismiss();
                alertDialog = null;
            }
        }
    }

    public void baseToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public int baseGetExtra() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) return extras.getInt("action");
        else return -1;
    }

    public void baseIntent(Class target, int extra) {
        Intent intent = new Intent(this, target);
        intent.putExtra("extra", extra);
        startActivityForResult(intent, extra);
    }

    public void baseIntent(Class target, String extra) {
        Intent intent = new Intent(this, target);
        intent.putExtra("extra", extra);
        startActivity(intent);
        finish();
    }

    public void baseIntent(Class target, boolean isFinish) {
        Intent intent = new Intent(this, target);
        startActivity(intent);
        if (isFinish) finish();
    }

    public void baseIntentService(Class target) {
        Intent intent = new Intent(this, target);
        startService(intent);
    }

    @SuppressLint("MissingPermission")
    public void baseIntentCall(String number) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number));
        startActivity(callIntent);
    }
}