package com.petruk.siakadpancasakti.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.petruk.siakadpancasakti.R;

import java.util.Objects;

/**
 * Created by angger on 04/11/18.
 */

public class BaseFragment extends Fragment {
    public FragmentActivity mContext;
    private AlertDialog alertDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = (FragmentActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @SuppressLint("InflateParams")
    public void baseLoading(boolean mode) {
        if (mode) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Dialog);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.app_loading, null);
            dialogBuilder.setView(dialogView);
            alertDialog = dialogBuilder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setCancelable(false);
            alertDialog.show();
        } else {
            if (alertDialog != null) {
                alertDialog.hide();
                alertDialog.dismiss();
                alertDialog = null;
            }
        }
    }

    public void errorMessage(String message) {
        baseLoading(false);
        toast(message);
    }

    public void intent(Class target) {
        Intent intent = new Intent(getActivity(), target);
        startActivity(intent);
        Objects.requireNonNull(getActivity()).finish();
    }

    public void intentService(Class target) {
        Intent intent = new Intent(getActivity(), target);
        Objects.requireNonNull(getActivity()).startService(intent);
    }

    public void toast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
