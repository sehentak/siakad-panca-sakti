package com.petruk.siakadpancasakti.base;

import com.google.firebase.auth.FirebaseAuth;
import com.petruk.siakadpancasakti.helper.StringHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BasePresenter<V> {
    protected V mView;
    protected FirebaseAuth mAuth;

    public BasePresenter(V mView) {
        this.mView = mView;
    }

    public boolean validateEmail(String email) {
        String pattern = StringHelper.REGEX_EMAIL;
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public String removeDot(String email) {
        return email.replace(".", "SPS0");
    }
}