package com.petruk.siakadpancasakti.model;

public class ConnEmail {
    private int nim;

    public ConnEmail() {
        // empty construction
    }

    public ConnEmail(int nim) {
        this.nim = nim;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }
}