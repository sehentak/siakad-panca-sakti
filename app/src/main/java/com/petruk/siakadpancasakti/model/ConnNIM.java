package com.petruk.siakadpancasakti.model;

public class ConnNIM {
    private String email;
    private String password;

    public ConnNIM() {
        // empty construction
    }

    public ConnNIM(String email) {
        this.email = email;
    }

    public ConnNIM(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}