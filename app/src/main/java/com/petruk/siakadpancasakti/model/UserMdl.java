package com.petruk.siakadpancasakti.model;

public class UserMdl {
    private int nim;
    private String nama;
    private String tempatLahir;
    private String tanggalLahir;
    private String kelamin;
    private String statusSipil;
    private String agama;
    private int nik;
    private int anak;
    private int jumlahSaudara;
    private int beratBadan;
    private int tinggiBadan;
    private String kecamatan;
    private String kelurahan;
    private String kodePos;
    private String dusun;
    private int rt;
    private int rw;
    private String alamat;
    private int telepon;
    private String email;
    private int program;
    private int role;
    private String avatar;

    public UserMdl() {
        // empty cons
    }

    public UserMdl(int nim, String nama, String tempatLahir, String tanggalLahir, String kelamin,
                   String statusSipil, String agama, int nik, int anak, int jumlahSaudara,
                   int beratBadan, int tinggiBadan, String kecamatan, String kelurahan, String kodePos,
                   String dusun, int rt, int rw, String alamat, int telepon, String email,
                   int program, int role, String avatar) {
        this.nim = nim;
        this.nama = nama;
        this.tempatLahir = tempatLahir;
        this.tanggalLahir = tanggalLahir;
        this.kelamin = kelamin;
        this.statusSipil = statusSipil;
        this.agama = agama;
        this.nik = nik;
        this.anak = anak;
        this.jumlahSaudara = jumlahSaudara;
        this.beratBadan = beratBadan;
        this.tinggiBadan = tinggiBadan;
        this.kecamatan = kecamatan;
        this.kelurahan = kelurahan;
        this.kodePos = kodePos;
        this.dusun = dusun;
        this.rt = rt;
        this.rw = rw;
        this.alamat = alamat;
        this.telepon = telepon;
        this.email = email;
        this.program = program;
        this.role = role;
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getProgram() {
        return program;
    }

    public void setProgram(int program) {
        this.program = program;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAnak() {
        return anak;
    }

    public void setAnak(int anak) {
        this.anak = anak;
    }

    public int getBeratBadan() {
        return beratBadan;
    }

    public void setBeratBadan(int beratBadan) {
        this.beratBadan = beratBadan;
    }

    public int getJumlahSaudara() {
        return jumlahSaudara;
    }

    public void setJumlahSaudara(int jumlahSaudara) {
        this.jumlahSaudara = jumlahSaudara;
    }

    public int getNik() {
        return nik;
    }

    public void setNik(int nik) {
        this.nik = nik;
    }

    public int getRt() {
        return rt;
    }

    public void setRt(int rt) {
        this.rt = rt;
    }

    public int getTinggiBadan() {
        return tinggiBadan;
    }

    public void setTinggiBadan(int tinggiBadan) {
        this.tinggiBadan = tinggiBadan;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelamin() {
        return kelamin;
    }

    public void setKelamin(String kelamin) {
        this.kelamin = kelamin;
    }

    public int getRw() {
        return rw;
    }

    public void setRw(int rw) {
        this.rw = rw;
    }

    public String getDusun() {
        return dusun;
    }

    public void setDusun(String dusun) {
        this.dusun = dusun;
    }

    public String getStatusSipil() {
        return statusSipil;
    }

    public void setStatusSipil(String statusSipil) {
        this.statusSipil = statusSipil;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public int getTelepon() {
        return telepon;
    }

    public void setTelepon(int telepon) {
        this.telepon = telepon;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }
}