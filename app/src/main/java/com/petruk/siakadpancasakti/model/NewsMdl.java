package com.petruk.siakadpancasakti.model;

/**
 * Created by angger on 08/07/18.
 */

public class NewsMdl {
    private String title;
    private String message;
    private String image;
    private String date;
    private int type;

    public NewsMdl() {
//        empty constructor
    }

    public NewsMdl(String title, String message, String image, String date, int type) {
        this.title = title;
        this.message = message;
        this.image = image;
        this.date = date;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
