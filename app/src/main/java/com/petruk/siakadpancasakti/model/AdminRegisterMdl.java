package com.petruk.siakadpancasakti.model;

public class AdminRegisterMdl {
    private String nim;
    private String name;
    private int program;
    private int role;

    public AdminRegisterMdl() {
        // empty construction
    }

    public AdminRegisterMdl(String nim, String name, int program, int role) {
        this.nim = nim;
        this.name = name;
        this.program = program;
        this.role = role;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProgram() {
        return program;
    }

    public void setProgram(int program) {
        this.program = program;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}